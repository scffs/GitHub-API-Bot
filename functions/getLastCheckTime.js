const lastCheckTimes = {};
export const getLastCheckTime = (repo) => {
    const repoKey = `${repo.owner}/${repo.repoName}`;

    if (!lastCheckTimes.hasOwnProperty(repoKey)) {
        lastCheckTimes[repoKey] = new Date().getTime() - 24 * 60 * 60 * 1000;
    }

    return lastCheckTimes[repoKey];
}
export const updateLastCheckTime = (repo) => {
    const repoKey = `${repo.owner}/${repo.repoName}`;
    lastCheckTimes[repoKey] = new Date().getTime();
}