import {reposList} from '../reposList/reposList.js';
import {getNewCommits} from './getNewCommits.js';
import {sendToVk} from './sendToVK.js';

let currentRepoIndex = 0;
let count = 1;

export const getMessage = async () => {
    if (reposList.length === 0) {
        return;
    }

    const repo = reposList[currentRepoIndex];
    console.log(`Monitoring commits in ${repo.repoName}`);
    const newCommits = await getNewCommits(repo);

    for (const commit of newCommits) {
        const id = commit.sha;
        const repoName = `${repo.owner}/${repo.repoName}`;
        const refName = commit.commit.tree.sha;
        const commitMessage = commit.commit.message;
        const time = commit.commit.author.date;
        const message = `#${count}. Time [${time}]. Repo: [${repoName}]\nBranch [${refName}], ID [${id}]\nText: ${commitMessage}`;
        console.log(message);
        count++;
        await sendToVk(614665065, message);
    }

    currentRepoIndex++;
    if (currentRepoIndex >= reposList.length) {
        currentRepoIndex = 0;
    }
}