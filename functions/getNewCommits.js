import axios from 'axios';
import {config} from 'dotenv';

import {getLastCheckTime, updateLastCheckTime} from './getLastCheckTime.js';


const tokenGit = config().parsed.ACCES_TOKEN;

export const getNewCommits = async (repo) => {
    try {
        const lastCheckTime = getLastCheckTime(repo);
        const response = await axios.get(`https://api.github.com/repos/${repo.owner}/${repo.repoName}/commits`, {
            headers: {
                Authorization: `token ${tokenGit}`
            }
        });

        const newCommits = response.data.filter(commit => {
            const commitTime = new Date(commit.commit.author.date).getTime();
            return commitTime > lastCheckTime;
        });

        updateLastCheckTime(repo);

        return newCommits;
    } catch (error) {
        console.error('Error while checking commits:', error);
        return [];
    }
}