import {VK} from 'vk-io';
import {config} from 'dotenv';

const token = config().parsed.VK;

const vk = new VK({
    token: token
});

export const sendToVk = async (userId, message) => {
    try {
        await vk.api.messages.send({
            user_id: userId,
            message: message,
            random_id: Math.floor(Math.random() * 1e9)
        });
        console.log('Message sent');
    } catch (error) {
        console.error('Error while sending:', error);
    }
}