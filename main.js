import {getMessage} from './functions/getMessage.js';

const checkCommits = () => setInterval(getMessage, 30000);

checkCommits();